fun main() {
    fun WordsScore(input: String): String {
        // split input into a list
        var words = input.split(" ")
        var max = 0
        var answer = ""
        //computing
        for (item in words) {
            var score = 0
            for(index in 0..item.length-1){
                score += item[index] - 'a' + 1;
            }
            // check the sum of words 
            if(score > max){
                answer = item
                max = score
            }
        } 
        return answer 
    }
    println(WordsScore("this ttt is a word")) // expected ttt, (ttt == word) then the first will be displayed
}