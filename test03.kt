fun main() {
    fun ReverseString(input: String): String {
        // split input into a list
        var list = input.split(" ")
        var new = ""
        // reverse texts 
        for (index in 0..list.size - 1) {
            new += list[index].reversed()+ " "
        } 
        return new
    }
    print(ReverseString("double spaces")) //expected "elbuod secaps"
}