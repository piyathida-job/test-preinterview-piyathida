fun main() {
    fun SquareDigits(input: Int): Int {
        // split input into a list
        var list = input.toString().split("")
        var number = ""
        // computing
        for (index in 0..list.size - 1) {
            if (!list[index].equals("")) {
                var num = Integer.parseInt(list[index])
                number += (num * num).toString()
            }
        } 
        var cc = Integer.parseInt(number)
        return cc
    }
    print(SquareDigits(1250)) //expected 14250
}
