import java.util.Scanner
class OddEvenList<T> {
    private var elements: ArrayList<T> = ArrayList()
	constructor (list: ArrayList<T>){
		this.elements = list
	}
    // getting the odd indices from the list
    fun getOdd(): ArrayList<T>{
        var result: ArrayList<T> = ArrayList()
        for (i in 0 until this.elements.size) { 
            var value = this.elements[i]
            if (i%2==1) {
                result.add(value)
            }
        }
        return result
	}
    // getting the even indices from the list
    fun getEven(): ArrayList<T>{
        var result: ArrayList<T> = ArrayList()
        for (i in 0 until this.elements.size) {
            var value = this.elements[i]
            if (i%2==0) {
                result.add(value)
            }
        }
        return result
	}
    // adding the number to the list
    fun add(input : T) : Int{
        this.elements.add(input)
        return this.elements.lastIndex
    }
    // removing the number from the list by index
    fun remove(index: Int): Boolean{
        if(index <= this.elements.size-1){
            this.elements.removeAt(index)
            return true
        }else{
            return false
        }
        
    }
    fun getList(): ArrayList<T>{
        return elements
	}   
}
fun main() {

    println("-----------------------------------------------------------")
    var list1 = OddEvenList(arrayListOf<String>("zero", "one", "two", "three", "four" )) // text list
    println("List: "+list1.getList())
    println("the odd: "+list1.getOdd())
    println("the even: "+list1.getEven())

    // adding
    println("0. add at index: "+ list1.add("five"))
    println("   List after adding: "+list1.getList())

    // remove: fail
    println("1. remove status: "+ list1.remove(55))
    println("   List after removing: "+list1.getList())

    // remove: pass 
    println("2. remove status: "+ list1.remove(2))
    println("   List after removing: "+list1.getList())

    
    
    println("-----------------------------------------------------------")
    var list2 = OddEvenList(arrayListOf<Int>(0, 1, 2, 3, 4)) // number list
    println("List: "+list2.getList())
    println("the odd: "+list2.getOdd())
    println("the even: "+list2.getEven())
    
    // adding
    println("0. add at index: "+ list2.add(5))
    println("   List after adding: "+list2.getList())

     // remove: fail 
    println("1. remove status: "+ list2.remove(55))
    println("   List after removing: "+list2.getList())

    // remove: pass 
    println("2. remove status: "+ list2.remove(2))
    println("   List after removing: "+list2.getList())



}